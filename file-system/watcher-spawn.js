"use strict";
const fs = require('fs');
const filename = process.argv[2];
const spawn = require('child_process').spawn;

if(!filename){
    throw Error("A file nmust be specified")
}
fs.watch(filename, function() {
    let ls = spawn('ls', ['-lh', filename]);
    ls.stdout.pipe(process.stdout);
    
    //console.log('File "'+ filename +'" just changed !!');
});
console.log('Watching  "'+filename+'"  for changes !!');